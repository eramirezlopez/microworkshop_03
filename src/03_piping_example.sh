#!/bin/bash

# Mostrando archivos
# head ./assets/celestina.txt
# head -100 ./assets/celestina.txt
# tail ./assets/celestina.txt
# cat ./assets/celestina.txt

# Buscando en archivos
# cat ./assets/celestina.txt | grep "Melibea";
# cat ./assets/*.txt | grep "padrino";

# Haciendo backups de archivos
# DESTINO=./assets/library_backup_$(date +%Y%m%d).tar.gz
# tar -czf "$DESTINO" ./assets/*.txt

# Sustituyendo en archivos
# cp ./assets/el_quijote.txt ./assets/el_Cipriano.txt
# sed s/QUIJOTE/CIPRIANO/ ./assets/el_quijote.txt 1> ./assets/el_Cipriano.txt;