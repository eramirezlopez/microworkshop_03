#!/bin/bash

# Create a script that takes any picture passed as parameter and converts it to
# resolution 400x400. It must also include a watermark in low-right corner of Barbara Logo.

# HINT:
# Search for "resize", "overlay" and "crop" in ffmpeg filters.