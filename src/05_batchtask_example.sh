#!/bin/sh

# Watermarking images
# for f in ./assets/pictures/*; 
# do ffmpeg -i "$f" -i ./assets/barbara_logo.png -filter_complex "overlay=10:10" "${f%.jpg}.png";
# done

# Creating a meme GIF
# ffmpeg -i ./assets/la_mereterica.mp4 -filter_complex "[0:v] fps=12,scale=-1:400,split [a][b];[a] palettegen [p];[b][p] paletteuse" ./assets/la_mereterica.gif
# ffmpeg -i ./assets/la_mereterica.gif -i ./assets/meme_texto.png -filter_complex "overlay=150:" ./assets/la_mereterica.gif -y;