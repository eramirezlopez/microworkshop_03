#!/bin/bash

# Receiving one parameter
echo "Hello $1";

# Receiving some determinate parameters
# echo "Hello $1 - $2 - $3 - $4 >>"

# Receiving some indeterminate parameters
# for i in "$@"
# do
#     echo "Script arg is $i"
# done

# Checking number of total received parameters
# echo "Arg Number is $#"