#!/bin/bash

# Write a bash script that create a list of branches in actual repository
# the name of the branches should be passed via parameters.
# For every branch, it should create a new file named "newfile.txt", add it 
# to a commit and push it to remote repo.